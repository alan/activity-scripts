require 'date'
require 'json'
require 'net/http'
require 'open3'

ACTIVITY_NOTE_COMMENT = '<!-- activity_note -->'
ACTIVITY_PERIOD = 7
PER_PAGE = 100
PAGE_LIMIT = 5
IGNORED_PROJECT_ID = []
IGNORED_PATHS = %w[gitlab-org/govern/security-policies]
GROUPS = %w[gitlab-com gitlab-org]
PROJECTS = %w[gitlab-org/gitlab]
PROJECT_ID_TO_URL = {
  278_964 => 'gitlab-org/gitlab',
  7764 => 'gitlab-com/www-gitlab-com',
  32_950_782 => 'gitlab-org/editor-extensions/gitlab-jetbrains-plugin'
}
TARGET_TYPES_TO_REFERENCE = {
  'MergeRequest' => '!',
  'Issue' => '#',
  'Epic' => '&'
}
TARGET_TYPES_TO_ROUTE = {
  'MergeRequest' => 'merge_requests',
  'Issue' => 'issues',
  'Epic' => 'epics'
}
FILE_NAME = 'body.md'
DEFAULT_ISSUE_IID = 1
UNKNOWN_LABEL = { 'title' => 'unknown' }
DATE_FORMAT = ->(date) { date.strftime('%F') }
EVENTS_API_URL = lambda { |user_id, date_from, date_to|
  "users/#{user_id}/events?after=#{DATE_FORMAT.call(date_from)}&before=#{DATE_FORMAT.call(date_to)}"
}
NEW_VERSION_WARNING = 'new version of glab has been released'

GlabError = Class.new(StandardError)

def format_date(date)
  date.strftime('%F')
end

def merge_request_graphql_query(group_path, date_from, date_to, username)
  <<~GRAPHQL
    query {
      group(fullPath: "#{group_path}") {
        mergeRequests(createdAfter: "#{DATE_FORMAT.call(date_from)}", createdBefore: "#{DATE_FORMAT.call(date_to)}", authorUsername: "#{username}", first: #{PER_PAGE}, includeSubgroups: true) {
          nodes {
            webUrl
            createdAt
            project {
              fullPath
            }
          }
        }
      }
    }
  GRAPHQL
end

def currently_assigned_issues_graphql_query(project_path, username)
  <<~GRAPHQL
    query {
      project(fullPath: "#{project_path}") {
        issues(assigneeUsernames: ["#{username}"], first: #{PER_PAGE}, state: opened) {
          nodes {
            webUrl
            createdAt
            milestone {
              title
              dueDate
            }
            labels {
              nodes {
                title
              }
            }
          }
        }
      }
    }
  GRAPHQL
end

def call_glab_api(command)
  stdout, stderr, status = Open3.capture3("glab api #{command}")
  return stdout if status.success? && (stderr.strip.empty? || stderr.include?(NEW_VERSION_WARNING))

  raise GlabError, stderr
end

def respond_with_error(method_name, error)
  puts "Error in ##{method_name} [#{error.class}: #{error.message.strip}]"
  puts '...failed.'
  exit 1
end

def collect_and_group_events(user_id, date_from, date_to)
  events = fetch_events(user_id, date_from, date_to)
  raw_events = events

  events
    .uniq { |event| event['id'] }
    .sort_by { |event| event['created_at'] }
    .group_by { |event| event['action_name'] }
    .then { |events| [events, raw_events] }
end

def find_label_and_extract_category(project, category = nil)
  category_label = project.dig('labels', 'nodes').find { |label| label['title'].start_with?("#{category}::") }
  return UNKNOWN_LABEL['title'] if category_label.nil?

  category_label['title'].split('::').last
end

def fetch_previous_workflow_label(issue, current_workflow_label, issues_with_workflow_from_last_update)
  return '' if issues_with_workflow_from_last_update.nil?

  previous_workflow_label = issues_with_workflow_from_last_update[issue['webUrl']]

  return if previous_workflow_label.nil? || previous_workflow_label.empty?
  return ' (:warning: *no updates*)' if current_workflow_label == previous_workflow_label

  " (previous: `#{previous_workflow_label})`"
end

def collect_currently_assigned_issues(username, issues_with_workflow_from_last_update = {})
  issues_with_workflow = {}

  PROJECTS
    .flat_map do |project_path|
      fetch_graphql_api(currently_assigned_issues_graphql_query(project_path, username)).dig(
        'data', 'project', 'issues', 'nodes'
      )
    end
    .sort_by { |issue| issue.dig('milestone', 'dueDate') || '2099-12-31' }
    .group_by { |issue| issue.dig('milestone', 'title') || 'Unknown' }
    .map do |milestone, issues|
      issues_markdown = issues.map do |issue|
        workflow_label = find_label_and_extract_category(issue, 'workflow')
        type_label = find_label_and_extract_category(issue, 'type')
        deliverable_stretch_label = issue.dig('labels', 'nodes').find { |label| %w[Stretch Deliverable].include?(label['title']) } || UNKNOWN_LABEL

        previous_workflow_label = fetch_previous_workflow_label(issue, workflow_label, issues_with_workflow_from_last_update)
        issues_with_workflow[issue['webUrl']] = workflow_label

        <<~MARKDOWN
          * #{issue['webUrl']}+ (`#{issue['createdAt']}`)
             * workflow: `#{workflow_label}`#{previous_workflow_label} | type: `#{type_label}` | `#{deliverable_stretch_label['title']}`
        MARKDOWN
      end

      <<~MARKDOWN
        ### `#{milestone}`: #{issues.size}

        #{issues_markdown.join}
      MARKDOWN
    end.join("\n") + "<!-- assigned_issues:[#{issues_with_workflow.to_json}] -->\n"
end

def collect_merge_requests(username, date_from, date_to)
  GROUPS
    .flat_map do |group_path|
    fetch_graphql_api(merge_request_graphql_query(group_path, date_from, date_to, username)).dig(
      'data', 'group', 'mergeRequests', 'nodes'
    )
  end
    .reject { |merge_request| IGNORED_PATHS.any? { |path| merge_request.dig('project', 'fullPath').to_s.include?(path) } }
    .sort_by { |merge_request| merge_request['createdAt'] }
    .group_by { |merge_request| merge_request.dig('project', 'fullPath') }
    .map do |group_path, merge_requests|
      merge_requests_markdown = merge_requests.map { |mr| "- [ ] #{mr['webUrl']}+ (`#{mr['createdAt']}`)" }.join("\n")
      <<~MARKDOWN
        ### `#{group_path}`: #{merge_requests.size}

        #{merge_requests_markdown}
      MARKDOWN
    end.join("\n")
end

def fetch_api(url)
  raw = call_glab_api(url)
  JSON.parse(raw)
rescue StandardError => e
  respond_with_error(:fetch_api, e)
end

def fetch_graphql_api(graqhql_query)
  raw = call_glab_api("graphql -f query='#{graqhql_query}'")
  JSON.parse(raw)
rescue StandardError => e
  respond_with_error(:fetch_graphql_api, e)
end

def fetch_events(user_id, date_from, date_to)
  raw = call_glab_api("--paginate #{EVENTS_API_URL.call(user_id, date_from, date_to)}")
  JSON.parse(raw.gsub('][', ','))
rescue StandardError => e
  respond_with_error(:fetch_events, e)
end

def prepare_url(title, project_id, type, iid, note_id = nil, url_only: false)
  if PROJECT_ID_TO_URL.key?(project_id)
    target_type = TARGET_TYPES_TO_REFERENCE[type]

    if note_id.nil?
      "#{PROJECT_ID_TO_URL[project_id]}#{target_type}#{iid}+"
    else
      "https://gitlab.com/#{PROJECT_ID_TO_URL[project_id]}/-/#{TARGET_TYPES_TO_ROUTE[type]}/#{iid}#note_#{note_id}"
    end
  else
    target_type = TARGET_TYPES_TO_ROUTE[type]
    api_url = "https://gitlab.com/api/v4/projects/#{project_id}/#{target_type}/#{iid}"
    api_response = fetch_api(api_url)
    note_suffix = note_id.nil? ? nil : "#note_#{note_id}"
    web_url = "#{api_response['web_url']}#{note_suffix}"
    return web_url if url_only

    "[#{title}](#{web_url})"
  end
end

def prepare_commit_url(project_id, sha)
  api_url = "https://gitlab.com/api/v4/projects/#{project_id}"
  api_response = fetch_api(api_url)

  "#{api_response['web_url']}/-/commit/#{sha}"
end

def merge_request_events_with_action(events, action)
  events
    .fetch(action, [])
    .select { |event| event['target_type'] == 'MergeRequest' }
    .map do |event|
    "- [ ] #{prepare_url(event['target_title'], event['project_id'], 'MergeRequest',
                         event['target_iid'])} (`#{event['created_at']}`)"
  end
    .join("\n")
end

def issue_events_with_action(events, action)
  events
    .fetch(action, [])
    .select { |event| event['target_type'] == 'Issue' || event['target_type'] == 'Epic' || event['target_type'] == 'WorkItem' }
    .map do |event|
    "- [ ] #{prepare_url(event['target_title'], event['project_id'], 'Issue',
                         event['target_iid'])} (`#{event['created_at']}`)"
  end
    .join("\n")
end

EVENTS_BY_TARGET_TYPE = {
  'MergeRequest' => %w[opened approved accepted closed],
  'Issue' => %w[opened closed]
}

def prepare_merge_requests(events)
  EVENTS_BY_TARGET_TYPE['MergeRequest'].map do |action|
    action_events = merge_request_events_with_action(events, action)
    next if action_events.empty?

    <<~MARKDOWN
      #### #{action.capitalize} Merge Requests

      #{action_events}
    MARKDOWN
  end.compact.join("\n")
end

def prepare_issues(events)
  EVENTS_BY_TARGET_TYPE['Issue'].map do |action|
    action_events = issue_events_with_action(events, action)
    next if action_events.empty?

    <<~MARKDOWN
      ### #{action.capitalize} Issues

      #{action_events}
    MARKDOWN
  end.compact.join("\n")
end

def prepare_comments(events)
  events
    .fetch('commented on', [])
    .reject { |event| IGNORED_PROJECT_ID.include?(event['project_id']) }
    .group_by { |event| event['target_title'] }
    .map do |title, events|
      first_event = events.first
      formatted_events = events.map do |event|
        [
          "   - [ ] [`#{event['created_at']}`](#{prepare_url(title, first_event['project_id'],
                                                             first_event.dig('note', 'noteable_type'), first_event.dig('note', 'noteable_iid'), event['target_iid'], url_only: true)}):",
          event.dig('note', 'body').split("\n").map { |line| "      > #{line}" }.join("\n")
        ].join("\n")
      end.join("\n\n")

      <<~MARKDOWN
        * commented in #{prepare_url(title, first_event['project_id'], first_event.dig('note', 'noteable_type'), first_event.dig('note', 'noteable_iid'))}:
        #{formatted_events}
      MARKDOWN
    end.join("\n")
end

def prepare_pushes(events)
  events
    .values_at('pushed new', 'deleted', 'pushed to')
    .flatten
    .compact
    .sort_by { |event| event['created_at'] }
    .map do |event|
      commit_details = if event.dig('push_data',
                                    'commit_title').nil?
                         nil
                       else
                         "[#{event.dig('push_data',
                                       'commit_title')}](#{prepare_commit_url(
                                         event['project_id'], event.dig(
                                                                'push_data', 'commit_to'
                                                              )
                                       )}) "
                       end
      "- [ ] **#{event['action_name']}** `#{event.dig('push_data',
                                                      'ref_type')}` `#{event.dig('push_data',
                                                                                 'ref')}` #{commit_details}`#{event['created_at']}`"
    end.join("\n")
end

def fetch_last_note(project_id, issue_iid)
  fetch_api("projects/#{project_id}/issues/#{issue_iid}/notes?order_by=created_at&sort=desc&per_page=10")
    .find { |note| note['body'].include?(ACTIVITY_NOTE_COMMENT) }
end

def parse_issues_with_workflow_from_last_update(last_note)
  return {} if last_note.nil?

  issues_json = last_note['body'].match(/<!-- assigned_issues:\[(?<issues_json>.*)\] -->/)&.[](:issues_json)
  return {} if issues_json.nil? || issues_json.empty?

  JSON.parse(issues_json)
rescue JSON::ParserError
  {}
end

def prepare_note(username, user_id, date_to, last_note)
  date_30_days_ago = date_to.prev_day(30)
  date_from = date_to.prev_day(ACTIVITY_PERIOD)
  events, _raw_events = collect_and_group_events(user_id, date_from, date_to)
  issues_with_workflow_from_last_update = parse_issues_with_workflow_from_last_update(last_note)
  currently_assigned_issues = collect_currently_assigned_issues(username, issues_with_workflow_from_last_update)
  mrs_from_last_week = collect_merge_requests(username, date_from, date_to)
  mrs_from_last_month = collect_merge_requests(username, date_30_days_ago, date_to)

  <<~MARKDOWN
    #{ACTIVITY_NOTE_COMMENT}

    <details>
    <summary>
    <strong>Activity for #{DATE_FORMAT.call(date_from)} - #{DATE_FORMAT.call(date_to)}</strong>
    </summary>

    ## Currently assigned issues

    #{currently_assigned_issues}

    ## Merge Requests

    ### MRs created within last 7 days

    #{mrs_from_last_week}

    ### MRs created within last 30 days

    #{mrs_from_last_month}

    ### Merge Request Activities

    #{prepare_merge_requests(events)}

    ## Pushes

    #{prepare_pushes(events)}

    ## Issues

    #{prepare_issues(events)}

    ## Comments

    #{prepare_comments(events)}

    <details>
    <summary>
    <i>Generated by [🧰 Activity Scripts](https://gitlab.com/alan/activity-scripts/) (<strong>v1.4.0</strong>)</i>
    </summary>

    ##### Used APIs

    ```
    * https://gitlab.com/api/v4/#{EVENTS_API_URL.call(user_id, date_from, date_to)}
    ```

    ##### GraphQL Queries

    * fetch currently assigned issues in gitlab-org/gitlab
    ```graphql
    #{currently_assigned_issues_graphql_query('gitlab-org/gitlab', username)}
    ```
    * fetch MRs from last 7 days
    ```graphql
    #{merge_request_graphql_query('gitlab-org', date_from, date_to, username)}
    ```
    * fetch MRs from last 30 days
    ```graphql
    #{merge_request_graphql_query('gitlab-org', date_30_days_ago, date_to, username)}
    ```
    </details>
    </details>
  MARKDOWN
end

def create_note(project_id, username, user_id, date_to, issue_iid)
  last_note = fetch_last_note(project_id, issue_iid)
  note = prepare_note(username, user_id, date_to, last_note)
  File.open(FILE_NAME, 'w+') { |f| f.write(note) }
  call_glab_api("projects/#{project_id}/issues/#{issue_iid}/notes -X POST -F body=@#{FILE_NAME}")
rescue StandardError => e
  respond_with_error(:create_note, e)
end

puts "Creating note: create_note(#{ENV['CI_PROJECT_ID']}, #{ENV['ACTIVITY_USERNAME']}, #{ENV['ACTIVITY_USER_ID']}, #{DATE_FORMAT.call(Date.today.prev_day)}, #{ENV['ACTIVITY_ISSUE_IID'] || DEFAULT_ISSUE_IID})..."
create_note(ENV['CI_PROJECT_ID'], ENV['ACTIVITY_USERNAME'], ENV['ACTIVITY_USER_ID'], Date.today.prev_day,
            ENV['ACTIVITY_ISSUE_IID'] || DEFAULT_ISSUE_IID)
puts '...done.'
