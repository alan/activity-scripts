# security-policies-scripts

This project contains scripts used in the Team Member Activity issue in the Security Policies group, although you can use it for every group.

## Instructions

1. [Create new project](https://gitlab.com/projects/new#blank_project)
1. In your new project, [create an access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token) with a `Reporter` role and `api` scope. You will use the value of the token in the following steps.
1. [Add these CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#for-a-project) to your project:
  * `GITLAB_TOKEN`: The token value you created in the previous step. Mark the variable as masked and protected.
  * `ACTIVITY_USER_ID`: The id of the user you'd like to record activity from. This can be found using API with https://gitlab.com/api/v4/users?username=alan, or when you want to get your user ID, you can use https://gitlab.com/api/v4/user.
  * `ACTIVITY_USERNAME`: The username of the user you'd like to record activity from.
1. Create a `.gitlab-ci.yml` file with the following contents:
   ```
   include:
   - https://gitlab.com/alan/activity-scripts/-/raw/main/templates/.gitlab-ci.yml
   ```
1. Make sure your project has an issue matching the `DEFAULT_ISSUE_IID` from `em_report.rb` (i.e., create an issue with ID 1)
1. [Create a new pipeline schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule) (weekly) and add the variable `CREATE_ACTIVITY_NOTE` set to `true`.
1. Test your activity tracking project by [running the schedule pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#run-manually). You should see data in the issue matching the `DEFAULT_ISSUE_IID` from `em_report.rb`.
